﻿
#include <iostream>
#include <cmath>

//test 2
template <typename T>
class MyDataclass
{
private:
    int rows;
    int cols;
    T** Arr;

    int* Data;
    std::string* Comment;

public:
    MyDataclass(int rows, int cols)
    {
        this->rows = rows;
        this->cols = cols;

        T** Arr = new T* [rows];
        
        for (int i = 0; i < rows; i++)
        {
            Arr[i] = new T[cols];

            for (int j = 0; j < cols; j++)
            {
                std::cin >> Arr[i][j];
            }
        }

        int* Data = new int(1);
        std::string* Comment = new std::string("Comment1");
        
        for (int i = 0; i < rows; i++)
        {
            delete[] Arr[i];
        }

    }

    MyDataclass& operator=(MyDataclass& other)
    {
        rows = other.rows;
        cols = other.cols;

        T** Arr = new T* [rows];
        for (int i = 0; i < rows; i++)
        {
            Arr[i] = new T[cols];

            for (int j = 0; j < cols; j++)
            {
               Arr[i][j] = other.Arr[i][j];
            }
        }

        int* Data = new int(*(other.Data));

        if (other.Comment)
        {
            if (Comment) delete Comment;
            Comment = new std::string(*(other.Comment));
        }

        return(*this);
    }
    
    ~MyDataclass()
    {
      
        delete[] Arr;
        delete Data;
        delete Comment;
    }
};
//test 2 end

//test 1
/*class Vector
{
public:
    Vector()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    Vector(float x, float y, float z)
        {
            this->x = x;
            this->y = y;
            this->z = z;
        }

    operator float()
    {
        return sqrt(x * x + y * y + z * z);
    }

    friend Vector operator+(const Vector& a, const Vector& b);

    friend std::ostream& operator<<(std::ostream& out, const Vector& v);

    friend bool operator>(const Vector& a, const Vector& b);

    friend Vector operator*(const Vector& a, const int c);

    friend Vector operator-(const Vector& a, const Vector& b);

    friend std::istream& operator>>(std::istream& in, Vector& v);

    float operator[](int index)
    {
        switch (index)
        {
        case 0:
            return x;
            break;
        case 1:
            return y;
            break;
        case 2:
            return z;
            break;
        default:
            std::cout << "index error";
            return 0;
            break;
        }
    }

  
private:
    float x;
    float y;
    float z;
};

bool operator>(const Vector& a, const Vector& b)
{
    return false;
}

Vector operator+(const Vector& a, const Vector& b)
{
    return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}

std::ostream& operator<<(std::ostream& out, const Vector& v)
{
    out << ' ' << v.x << ' ' << v.y << ' ' << v.z;
    return out;
}

Vector operator*(const Vector& a, const int c)
{
    return Vector(a.x * c, a.y * c, a.z * c);
}

Vector operator-(const Vector& a, const Vector& b)
{
    return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}

std::istream& operator>>(std::istream& in, Vector& v)
{
    in >> v.x >> v.y >> v.z;
    return in;
}*/
//test 1 end

int main()
{
    
    //Test 2
    int r;
    int c;
    std::cout << "Enter rows: " << '\t';
    std::cin >> r;
    std::cout << "enter columns: " << '\t';
    std::cin >> c;

    MyDataclass<int> Class1(r, c);
    MyDataclass<int> Class2 = Class1;

        // test 1
    /*Vector v1(1, 2, 3);
    Vector v2(2, 3, 4);
    Vector v3;
    Vector v4;
    v3 = v1 + v2;
    int c = 6;
    std::cout << v1 + v2 << '\n';
    std::cout << "vector lenght " << float(v3) << '\n';
    std::cout << v1 * c << '\n';
    std::cout << v2 - v1 << '\n';
    std::cin >> v4;
    std::cout << v4;*/
    
}

